#import "template.typ": template

#show: template.with(
  uv_name: "AIXX",
  title_name: "Rapport de projet",
  authors: ("Alice Polnareff", "Bob Lee"),
  date: none,
  list_of_tables: true,
)

= Titre de Votre Document
== Sous-titre
=== Sous Sous-titre
==== On peut vraiment continuer longtemps
$pi=4$

*Haha, c'est en gras.*

_Et là, c'est en italique._

/ PWM : Modulation de Largeur d'Impulsion

`Bitcoin to the moon`

