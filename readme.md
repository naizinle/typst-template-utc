# README

La documentation complète de ce projet se trouve dans le fichier **readme.pdf**. Veuillez cliquer sur le lien ci-dessous pour accéder au fichier PDF :

[**Lien vers README PDF**](readme.pdf)

Veuillez vous référer au fichier PDF pour obtenir des instructions détaillées sur l'installation, l'utilisation et d'autres détails importants concernant ce projet.
