#import "@preview/codelst:1.0.0": sourcecode
#import "template.typ": template

#let code(caption: none, body) = {
  figure(caption: caption)[
    #align(left)[
      #sourcecode(body)
    ]
  ]
}

#show: template.with(
  uv_name: link("https://typst.app/home")[Typst],
  title_name: "Réalisation de documents avec le template UTC",
  authors: ("Cécile Gaultier", "Yaniss Khima"),
)

= Introduction

Typst est un langage de formation de documents PDF, offrant une alternative à LaTeX. Conçu en Rust, ce logiciel incarne l'efficacité et la rapidité. Son binaire compact, occupant moins de 20 Mo, cache une puissance impressionnante. Typst se distingue par sa vitesse de compilation du PDF, qui est exceptionnellement rapide.

Le site officiel de typst est disponible à cette adresse : *https://typst.app/home * 


Ce qui rend Typst unique, c'est sa syntaxe polyvalente, combinant le Markdown, le LaTeX et le CSS. Cette combinaison intelligente permet aux utilisateurs de bénéficier du meilleur de chaque langage, simplifiant ainsi le processus de rédaction et de mise en page.

Typst offre également la possibilité de créer des fonctions et des blocs personnalisés, facilitant ainsi la mise en forme complexe des documents. Que vous soyez un étudiant rédigeant un essai, un chercheur élaborant un article scientifique ou un professionnel produisant un rapport détaillé, Typst vous permet d'exprimer vos idées de manière claire et professionnelle, tout en garantissant une présentation impeccable et rapide. Prêt à explorer ce nouvel horizon de la rédaction ? Plongeons dans le monde dynamique de Typst.

= Installation
Pour installer Typst sur votre système, suivez ces étapes simples en fonction de votre plateforme.
== Linux
Consultez la page de Typst sur Repology pour les instructions spécifiques à votre distribution :

https://repology.org/project/typst/versions

== MacOS
+ Ouvrez le `Finder` (icône de dossier dans le dock).
+ Allez dans Applications > Utilitaires.
+ Vous y trouverez l'application Terminal. Double-cliquez pour l'ouvrir.
+ Utilisez Homebrew en entrant dans votre terminal:
#code[```sh brew install typst```] 

== Windows

+ Cliquez sur l'icône de recherche dans la barre des tâches.
+ Tapez "cmd" ou "Command Prompt".
+ Appuyez sur Entrée pour ouvrir le terminal. 
+ Utilisez Winget avec la commande suivante :
#code[```sh winget install --id Typst.Typst```]

= Utilisation

Une fois que vous avez installé Typst, vous pouvez l'utiliser comme suit.

#code[
```sh
typst compile main.typ
```
]

Pour créer un fichier PDF à un chemin spécifique :

#code[
```sh
typst compile chemin/vers/source.typ chemin/vers/sortie.pdf
```
]

Typst vous permet également de surveiller vos fichiers source et de recompiler automatiquement en cas de modifications. Cela est plus rapide que de compiler à partir de zéro à chaque fois, car Typst utilise la compilation incrémentielle.

Pour surveiller les fichiers source et recompiler en cas de modifications :

#code[
```sh
typst watch file.typ
```
]

= Création d'un fichier Typst

La syntaxe de Typst est bien documentée, la documentation officiel est disponible à l'adresse suivante : *https://typst.app/docs *.

Le template Typst UTC est un modèle prédéfini qui peut être utilisé pour structurer vos documents. Il utilise des fonctions et leurs valeurs par défaut pour simplifier la création de documents cohérents et professionnels.

Le template Typst UTC est disponible à cette adresse : #parbreak()
*https://gitlab.utc.fr/naizinle/typst-template-utc *

 Voici comment vous pouvez utiliser le template UTC en modifiant le fichier `main.typ` avec les fonctions et leurs valeurs par défaut :

#code[
```typ
#let template(
  uv_name: none,
  title_name: none,
  authors: ("",),
  date: none,
  list_of_tables: true,
)
```
]

Dans ce modèle, vous pouvez personnaliser différentes parties de votre document en fournissant des valeurs spécifiques pour les paramètres. Voici ce que chaque paramètre signifie :

- `uv_name`: Le nom de l'UV (Unité de Valeur) ou un autre titre que vous souhaitez inclure dans votre document. Par défaut, il n'y a pas de valeur spécifiée (`none`).

- `title_name`: Le nom du document ou un titre que vous souhaitez afficher en haut du document. Par défaut, il n'y a pas de valeur spécifiée (`none`).

- `authors`: Les noms des auteurs du document. Par défaut, il n'y a pas de valeur spécifiée (`none`).

- `date`: La date à laquelle le document a été créé ou toute autre date pertinente que vous souhaitez inclure. Par défaut, la date d'aujourd'hui sera utilisé.

- `list_of_tables`: Un booléen indiquant si vous voulez inclure une table des matière dans votre document. Par défaut, il est défini sur `true`, ce qui signifie qu'une  table des matière sera incluse. Vous pouvez le personnaliser en le réglant sur `false` si vous ne voulez pas l'inclure.

Pour utiliser le modèle UTC dans votre document, vous pouvez l'importer en utilisant la syntaxe suivante :

#code[
```typ
#import "template.typ": *

#show: template.with(
  uv_name: "Nom de l'UV",
  title_name: "Titre de votre document",
  authors: ("Auteur 1", "Auteur 2"),
  date: "Date de création",
  list_of_tables: true,
)

// Contenu de votre document ici  

```
]

En remplaçant les valeurs des paramètres par celles que vous souhaitez dans votre document, vous pouvez personnaliser le modèle UTC selon vos besoins spécifiques.

= Contribution
Vous pouvez également contribuer au développement de Typst et aider à façonner l'avenir de cet outil prometteur.
Si vous souhaitez apporter votre contribution, vous pouvez le faire en devenant sponsor du projet sur https://github.com/sponsors/typst et en offrant votre soutien financier.

De plus, si vous êtes intéressé par la contribution directe au code, consultez le guide des contributeurs sur https://github.com/typst/typst. Ce guide détaille les étapes à suivre pour soumettre des correctifs, rapporter des problèmes ou proposer de nouvelles fonctionnalités.
Votre engagement peut aider Typst à évoluer et à s'améliorer.

= Conclusion
Félicitations ! Vous avez maintenant tous les outils en main pour prendre en main ce nouvel outil passionnant qu'est Typst.

N'hésitez pas à explorer ses fonctionnalités avancées, à personnaliser vos documents selon vos besoins spécifiques et à laisser libre cours à votre créativité. Que vous rédigiez des devoirs scolaires, des rapports professionnels ou des documents académiques, Typst vous accompagnera avec efficacité et professionnalisme.

Alors, lancez-vous ! Utilisez Typst pour créer des documents impeccables, et profitez de l'expérience de rédaction fluide et intuitive qu'il offre. Bonne rédaction !

= Réferences

- https://typst.app/home
- https://typst.app/docs/
- https://github.com/typst/typst
- https://github.com/qjcg/awesome-typsts
 
