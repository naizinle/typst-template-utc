#let utc_colors = (
   yellow: rgb(255, 217, 17),
   light_yellow: rgb(255,245,204),
   gray: rgb(89,89,89),
   light_gray: rgb(208,208,208),
   green: rgb(151,191,13),
   orange: rgb(236,117,46),
   blue: rgb(0,158,224),
)

#let title_page(
  uv_name: none,
  title_name: none,
  authors: none,
  date: datetime.today().display("[day] [month repr:long ] [year]"),
) = {

  align(center,{

    image("logos/UTC.svg", width: 7cm)

    align(horizon, {
      
      line(
        length: 100%,
        stroke: 0.8mm + utc_colors.yellow,
      )
  
      {
        set text(
          weight: "bold",
          size: 25pt,
        )
          
        if uv_name != none {
          uv_name
        }
    
        if uv_name != none and title_page != none {
          linebreak()
        }
        
        if title_name != none { 
          title_name
        }
      }
      line(length: 100%,
        stroke: 0.8mm + utc_colors.yellow,
      )

      {
        set text(size: 17pt)
        authors.join("\n")
    
        linebreak()    
        date
      }
      
    })
  })
  pagebreak()
}


#let french_months = (
  "January": "Janvier",
  "February": "Février",
  "March": "Mars",
  "April": "Avril",
  "May": "Mai",
  "June": "Juin",
  "July": "Juillet",
  "August": "Août",
  "September": "Septembre",
  "October": "Octobre",
  "November": "Novembre",
  "December": "Décembre",
)

#let template(
  uv_name: none,
  title_name: none,
  authors: ("",),
  date: none,
  list_of_tables : false,
  body
) = {


  //setting up the meta-data
  set document(title: title_name, author: authors.join(", "))

  set text(lang: "fr", region: "fr")
  
  if date == none {
    //Only english local is supported at the time
    let month = french_months.at(datetime.today().display("[month repr:long ]"))
    
    date = datetime.today().display("[day] " + month + " [year]")
  }
  
  
  title_page(
    uv_name: uv_name,
    title_name: title_name,
    authors: authors,
    date: date,
  )
  
  show par: set block(spacing: 1.3em)
  set par(
    first-line-indent: 1em,
    justify: true,
  )

  //Page config
  set page(
    
    header: locate(loc => {
      //Skip first page
      if loc.page() > 1 [
        #grid(
          columns: (1fr, 1fr, 1fr),
          rows: 60pt,
          {
            set align(left)
            smallcaps[#title_name]
          },
          {
            set align(center)
            image("logos/single_UTC.svg", width: 1.75cm)
          },
          {
            set align(right)
            [_#uv_name _]
          }
        )  
        
        #line(length: 100%,
        stroke: 2pt + utc_colors.yellow)
      ]
    }),

    footer: locate(loc => {
      if loc.page() > 1 [
        #line(length: 100%,
        stroke: 2pt + utc_colors.yellow)

        #grid(
          columns: (1fr, 1fr, 1fr),
          {
            set align(left)
            authors.join("\n")
          },
          {
            set align(center)
            counter(page).display()
          },
          {
            set align(right)
            //UNUSED
          }
        )    
      ]
    }),
  )


  show heading : it =>{
    if(it.level == 1){
      underline(
        stroke: 1.5pt + utc_colors.yellow,
        it
      )
    }

    else if (it.level == 2) {
      underline(
        stroke: 0.5pt + black,
        it
      )
    }

    else if (it.level == 3) {
      set text(fill: utc_colors.gray)
      it
    }

    else {
      parbreak()
      text(12pt, fill: utc_colors.gray, style: "italic", weight: "bold", it.body + ".")
      parbreak()
    }
  }
  
  set heading(numbering: "1.1.1.")  
  if list_of_tables {
    show outline.entry.where(level: 1): it => {  
        v(10pt, weak: true)
        strong(it)
    }

    ///
    /// Table of contents
    ///

    set par(first-line-indent: 0pt)

    outline(
      title: [Table des matières],
      depth: 3,
      indent: 12pt,
      fill: none,
    )

    pagebreak()
  }

  //The content is rendered here

  set par(first-line-indent: 0pt)

  body

  align(center, {
      set text(
        weight: "bold",
        size: 25pt,
      )
    [\* \* \*]
  })

}
